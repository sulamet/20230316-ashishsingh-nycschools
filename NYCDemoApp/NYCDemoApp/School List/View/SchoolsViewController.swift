//
//  SchoolsViewController.swift
//  NYCDemoApp
//
//  Created by Ashish Singh on 3/14/23.
//

import UIKit
import Combine

class SchoolsViewController: UIViewController {

    private enum SchoolListConstants: String {
        case title = "School List"
        case errorTitle = "Error"
        case errorBody = "Unable to fetch data. Try Later"
    }

    private var stackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .center
        stack.distribution = .fillEqually
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    private var tableView: UITableView = {
        let tableview = UITableView()
        tableview.translatesAutoresizingMaskIntoConstraints = false
        tableview.rowHeight = UITableView.automaticDimension
        tableview.estimatedRowHeight = 100.0
        tableview.backgroundColor = .black
        return tableview
    }()

    private var viewModel: ViewModelActionable
    private lazy var dataModelArray = [SchoolDataModel]()
    private var subscription = Set<AnyCancellable>()
    
    init(viewModel: SchoolsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.prefetchDataSource = self
        
        // bind output from view model
        bindOutput()
        
        viewModel.handleEvent(input: .fetchData)
    }
    
    func setUpView() {
        self.title = SchoolListConstants.title.rawValue
        view.addSubview(tableView)
        tableView.register(CustomTableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.backgroundColor = .systemGray
        setUpConstraints()
    }
    
    func setUpConstraints() {
        [tableView.topAnchor.constraint(equalTo: view.topAnchor),
         tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
         tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
         tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor)].forEach {
            $0.isActive = true
        }
    }
    
    // when view model emit any data on subject, it will captured here and UI will update
    func bindOutput() {
        viewModel.subject
            .receive(on: DispatchQueue.main)
            .sink { [weak self] completion in
                if case .failure(_) = completion {
                    self?.showError()
                }
            } receiveValue: { [weak self] result in
                self?.dataModelArray = result
                self?.tableView.reloadData()
            }
            .store(in: &self.subscription)

    }
    
    private func showError() {
        let alert = UIAlertController(title: SchoolListConstants.errorTitle.rawValue, message: SchoolListConstants.errorBody.rawValue, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak self] action in
            self?.navigationController?.popViewController(animated: true)
        }))
        present(alert, animated: true)
    }
}

extension SchoolsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataModelArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? CustomTableViewCell else { return UITableViewCell() }
        cell.contentView.backgroundColor = .black

        let data = dataModelArray[indexPath.row]
        cell.titleLabel.text = data.school_name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let data = dataModelArray[indexPath.row]
        if let dbnId = data.dbn {
            viewModel.handleEvent(input: .userSelection(dbnId: dbnId))
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


extension SchoolsViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        // TODO: instead of loading all data, it could be prefetched as user scroll
    }
}
