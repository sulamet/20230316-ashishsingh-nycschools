//
//  SchoolDetailViewModel.swift
//  NYCDemoApp
//
//  Created by Ashish Singh on 3/14/23.
//

import Foundation
import Combine

enum DetailInput {
    case fetchDetail
}

protocol DetailViewModelActionable {
    func handleEvent(input: DetailInput)
}

class SchoolDetailViewModel {
    private let flowController: FlowControllable
    private let networkManager: NetworkHandler
    var subject = PassthroughSubject<SchoolDetailDataModel, AppErrors>()
    var dataModel:  SchoolDetailDataModel
    
    // Dependency Injection
    init(flowController: FlowControllable, networkManager: NetworkHandler, dataModel: SchoolDetailDataModel) {
        self.flowController = flowController
        self.networkManager = networkManager
        self.dataModel = dataModel
    }
}

extension SchoolDetailViewModel: DetailViewModelActionable {
    func handleEvent(input: DetailInput) {
    }
}
