//
//  SchoolDetailViewController.swift
//  NYCDemoApp
//
//  Created by Ashish Singh on 3/14/23.
//

import UIKit

class SchoolDetailViewController: UIViewController {

    private enum SchoolListConstants: String {
        case title = "School Detail"
        case errorTitle = "Error"
        case errorBody = "Unable to fetch data. Try Later"
    }

    private var vStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .leading
        stack.spacing = 20
        stack.distribution = .fillEqually
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()

    private var mathHStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.alignment = .leading
        stack.spacing = 40
        stack.distribution = .fillEqually
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()

    private var readingHStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.alignment = .leading
        stack.spacing = 40
        stack.distribution = .fillEqually
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()

    private var writingHStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.alignment = .leading
        stack.spacing = 40
        stack.distribution = .fillEqually
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()

    // Leverage the build pattern to get the UI components
    // can get the customized ui element
    private let schoolName = UILabel.create(style: .normal())
    private let satScoreLabel = UILabel.create(with: "SAT SCORE", style: .large(color: .green))
    
    private let mathLabel = UILabel.create(with: "MATH", style: .normal())
    private let mathScore = UILabel.create(style: .normal(color: .white))
    
    private let readingLabel = UILabel.create(with: "READING", style: .normal())
    private let readingScore = UILabel.create(style: .normal())
    
    private let writingLabel = UILabel.create(with: "WRITING", style: .normal())
    private let writingScore = UILabel.create(style: .normal())

    private var viewModel: SchoolDetailViewModel
    private var dataModel: SchoolDetailDataModel?
    
    init(viewModel: SchoolDetailViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    func setUpView() {
        self.view.backgroundColor = .darkGray
        self.title = SchoolListConstants.title.rawValue
        mathScore.text = viewModel.dataModel.sat_math_avg_score
        readingScore.text = viewModel.dataModel.sat_critical_reading_avg_score
        writingScore.text = viewModel.dataModel.sat_writing_avg_score
        schoolName.text = viewModel.dataModel.school_name
        
        mathHStackView.addArrangedSubview(mathLabel)
        mathHStackView.addArrangedSubview(mathScore)

        readingHStackView.addArrangedSubview(readingLabel)
        readingHStackView.addArrangedSubview(readingScore)

        writingHStackView.addArrangedSubview(writingLabel)
        writingHStackView.addArrangedSubview(writingScore)

        vStackView.addArrangedSubview(schoolName)
        vStackView.addArrangedSubview(satScoreLabel)
        vStackView.addArrangedSubview(mathHStackView)
        vStackView.addArrangedSubview(readingHStackView)
        vStackView.addArrangedSubview(writingHStackView)
        
        view.addSubview(vStackView)
        
        setUpContraints()
    }
    
    func setUpContraints() {
        let guide = self.view.safeAreaLayoutGuide
        [vStackView.topAnchor.constraint(equalTo: guide.topAnchor, constant: 40),
         vStackView.bottomAnchor.constraint(equalTo: guide.bottomAnchor, constant: -50),
         vStackView.leftAnchor.constraint(equalTo: guide.leftAnchor, constant: 10),
         vStackView.rightAnchor.constraint(equalTo: guide.rightAnchor, constant: -10)]
            .forEach {
            $0.isActive = true
        }
        
        [mathHStackView.leftAnchor.constraint(equalTo: guide.leftAnchor, constant: 10),
         mathHStackView.rightAnchor.constraint(equalTo: guide.rightAnchor, constant: -10)]
            .forEach {
            $0.isActive = true
        }

        [readingHStackView.leftAnchor.constraint(equalTo: guide.leftAnchor, constant: 10),
         readingHStackView.rightAnchor.constraint(equalTo: guide.rightAnchor, constant: -10)]
            .forEach {
            $0.isActive = true
        }

        [writingHStackView.leftAnchor.constraint(equalTo: guide.leftAnchor, constant: 10),
         writingHStackView.rightAnchor.constraint(equalTo: guide.rightAnchor, constant: -10)]
            .forEach {
            $0.isActive = true
        }
    }
    
}
