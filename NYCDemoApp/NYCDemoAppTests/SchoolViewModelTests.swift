//
//  SchoolViewModelTests.swift
//  NYCDemoAppTests
//
//  Created by Ashish Singh on 3/15/23.
//

import XCTest
import Combine
@testable import NYCDemoApp

//******************************************************************************
// TODO: Test Schedular should be injected
// so that asynchronous events can be tested in synchronous way
//******************************************************************************

class SchoolViewModelTests: XCTestCase {

    var sut: SchoolsViewModel!
    var mockFlowController: MockFlowController!
    var mockNetworkManager: MockNetworkManager!
    var subscription: Set<AnyCancellable>!
    
    override func setUp() {
        subscription = Set<AnyCancellable>()
        mockFlowController = MockFlowController()
        mockNetworkManager = MockNetworkManager()
        sut = SchoolsViewModel(flowController: mockFlowController, networkManager: mockNetworkManager)
    }
    
    override func tearDown() {
        mockFlowController = nil
        mockNetworkManager = nil
        sut = nil
        subscription = nil
    }

    func testNetworkManagerIsConnected() throws {
        sut.handleEvent(input: .fetchData)
        XCTAssertTrue(mockNetworkManager.isFetchDataCalled)
    }
    
    func testHandleEventSuccess() {
        // Given
        let expected = SchoolDataModel.mock()
        mockNetworkManager.mockResult = .success(expected)

        sut.subject.sink { _ in } receiveValue: { result in
            // Then
            XCTAssertEqual(expected, result)
        }.store(in: &self.subscription)

        // When
        sut.handleEvent(input: .fetchData)
    }

    func testHandleEventFailed() {
        // Given
        let expectedError = AppErrors.network
        mockNetworkManager.mockResult = .failure(expectedError)

        sut.subject.sink { completion in
            // Then
            if case let .failure(error) = completion {
                XCTAssertEqual(error, expectedError)
            }
        } receiveValue: { _ in }
        .store(in: &self.subscription)

        // When
        sut.handleEvent(input: .fetchData)
    }
}


